data {
   //for fitting
  int N_free; //obs in the free promt data
  int NP1_free[N_free]; // referent == 1 in free promt data
  int<lower=1, upper = 3>pron_free[N_free]; // 1 is PP, 2 is DP, 3 is others
  int N_subj; // N subj in free prompt data
  int N_item; // total number of subject
  vector[N_free] ref_free; // either NP1 ==1, or NP2 ==-1 for each obs
  int<lower = 1, upper = N_subj> subj_free[N_free]; // indexes subjects
  int<lower = 1, upper = N_item> item_free[N_free]; // indexes items
  //for prediction
  int N_pron; //obs in the pronoun prompt data
  int NP1_pron[N_pron]; // referent == 1 in pronoun promt data
  int<lower = 1, upper = N_item> item_pron[N_pron]; // indexes items in the pronoun prompt data
  int<lower = 1, upper = N_subj> subj_pron[N_pron]; // indexes subjects
  vector[N_pron] ref_pron; // either NP1 ==1, or NP2 ==-1 for each obs
  int pp_pron[N_pron];
}
parameters {
  real<lower = 0, upper = 1> P_ref_NP1;
  real beta_ref[2];
  // Random effects:
  vector<lower = 0>[5]  tau_u;
  vector<lower = 0>[5]  tau_w;
  matrix[5, N_subj] z_u;
  matrix[5, N_item] z_w;
  cholesky_factor_corr[5] L_u;
  cholesky_factor_corr[5] L_w;
  real alpha[2];
}
transformed parameters {
  matrix[N_subj, 5] u;
  matrix[N_item, 5] w;
  vector[N_free] lo_pp;
  vector[N_free] lo_other;
  vector[N_free] lo_ref;
  real alpha_NP1 = logit(P_ref_NP1);
  u = (diag_pre_multiply(tau_u, L_u) * z_u)';
  w = (diag_pre_multiply(tau_w, L_w) * z_w)';
  //log odss of pronoun| referent , item, subject, verb type
    lo_pp = (alpha[1]  + u[subj_free, 1] + w[item_free,1]) +
    ref_free .* (beta_ref[1] + u[subj_free, 2] + w[item_free,2]);
  lo_other = (alpha[2]  + u[subj_free, 3] + w[item_free,3]) +
    ref_free .* (beta_ref[1] + u[subj_free, 4] + w[item_free,4]);

  lo_ref = (alpha_NP1 + u[subj_free, 5] + w[item_free,5]);

}
model {
  target += bernoulli_logit_lpmf(NP1_free | lo_ref);
 for(i in 1:N_free)
   target += categorical_logit_lpmf(pron_free[i]| [lo_pp[i], 0.0, lo_other[i]]' );  // Priors for f.e
  target += normal_lpdf(alpha| 0, 2);
  target += beta_lpdf(P_ref_NP1 | 1, 1);
  target += normal_lpdf(beta_ref | 0, 2);
  // Priors for r.e.
  target += normal_lpdf(tau_u | 0, 2) -
    5 * normal_lccdf(0 | 0, 2);
  target += normal_lpdf(tau_w | 0, 2) -
    5* normal_lccdf(0 | 0, 2);
  target += lkj_corr_cholesky_lpdf(L_u | 2);
  target += lkj_corr_cholesky_lpdf(L_w | 2);
  target += std_normal_lpdf(to_vector(z_u));
  target += std_normal_lpdf(to_vector(z_w));
}
generated quantities {
  corr_matrix[3] rho_u = L_u * L_u';
  corr_matrix[3] rho_w = L_w * L_w';
  int NP1_pred[N_pron];
  real loglik[N_pron];
  real P_ref_dat = inv_logit(alpha_NP1);

vector[3] pron_ref1_dat = softmax([
                                    alpha[1]   +
                                    beta_ref[1],
                                    0,
                                    alpha[2]   +
                                    beta_ref[2]]');

  vector[3] pron_ref2_dat = softmax([ alpha[1]   +
                                      -beta_ref[1],
                                      0,
                                      alpha[2]   +
                                      -beta_ref[2]]');

  real P_mirror_pp_dat = pron_ref1_dat[1]/(pron_ref1_dat[1] + pron_ref2_dat[1]);
  real P_mirror_dp_dat = pron_ref1_dat[2]/(pron_ref1_dat[2] + pron_ref2_dat[2]);


  real P_bayes_pp_dat = pron_ref1_dat[1] * P_ref_dat /(pron_ref1_dat[1]* P_ref_dat  + pron_ref2_dat[1] * (1- P_ref_dat));
  real P_bayes_dp_dat = pron_ref1_dat[2]* P_ref_dat/(pron_ref1_dat[2]* P_ref_dat + pron_ref2_dat[2]*(1- P_ref_dat));

{

  vector[N_pron] P_ref1 = inv_logit(alpha_NP1 + u[subj_pron, 3] + w[item_pron,3]);

for(n in 1:N_pron){
    vector[3] pron_ref1 = softmax([alpha[1]  + u[subj_pron[n], 1] + w[item_pron[n],1] +
     beta_ref[1] + u[subj_pron[n], 2]+ w[item_pron[n],2],
      0,
      alpha[2]  + u[subj_pron[n], 3] + w[item_pron[n],3] +
     beta_ref[2] + u[subj_pron[n], 4]+ w[item_pron[n],4]]'
    );

  vector[3] pron_ref2 = softmax( [alpha[1]  + u[subj_pron[n], 1] + w[item_pron[n],1] +
                                 - (beta_ref[1] + u[subj_pron[n], 2]+ w[item_pron[n],2]),
                                 0,
alpha[2]  + u[subj_pron[n], 3] + w[item_pron[n],3] +
                                 - (beta_ref[2] + u[subj_pron[n], 4]+ w[item_pron[n],4])
                                 ]') ;
  // "likelihood for personal pronoun"
    real P_pp = pron_ref1[1] * P_ref1[n] /(pron_ref1[1] * P_ref1[n] + pron_ref2[1]* (1-P_ref1[n]));
    real P_dp = pron_ref1[2]* P_ref1[n] /(pron_ref1[2]* P_ref1[n] + pron_ref2[2] * (1-P_ref1[n]));

        if(pp_pron[n]==1){
          loglik[n] = bernoulli_lpmf(NP1_pron[n] | P_pp);
          NP1_pred[n] = bernoulli_rng(P_pp);
        }else{
          loglik[n] = bernoulli_lpmf(NP1_pron[n] | P_dp);
          NP1_pred[n] = bernoulli_rng(P_dp);
        }
   }
  }
}
