data {
  //for fitting
  int N_free; //obs in the free promt data
  int NP1_free[N_free]; // referent == 1 in free promt data
  int N_subj; // N subj in free prompt data
  int N_item; // total number of subject
  int<lower = 1, upper = N_subj> subj_free[N_free]; // indexes subjects
  int<lower = 1, upper = N_item> item_free[N_free]; // indexes items
  //for prediction
  int N_pron; //obs in the pronoun prompt data
  int NP1_pron[N_pron]; // referent == 1 in pronoun promt data
  int<lower = 1, upper = N_item> item_pron[N_pron]; // indexes items in the pronoun prompt data
  int<lower = 1, upper = N_subj> subj_pron[N_pron]; // indexes subjects
}
parameters {
  real<lower = 0, upper = 1> P_ref_NP1; // P(referent==1)
  // Random effects:
  real<lower = 0>  tau_u; // by subject variance component
  real<lower = 0> tau_w; // by items variance component
  vector[N_subj] z_u;
  vector[N_item] z_w;
}
transformed parameters {
  vector[N_subj] u; // adjustments for subjects
  vector[N_item] w; // adjustment for item
  vector[N_free] lo_ref; //log odds of referent == 1
  real alpha_NP1 = logit(P_ref_NP1);
  u = tau_u * z_u;
  w = tau_w * z_w;
  lo_ref = (alpha_NP1 + u[subj_free] + w[item_free]);
  // glmer(ans ~ vtype + (1|item_free) + (vtype | subj_free))
}
model {
  target += bernoulli_logit_lpmf(NP1_free | lo_ref);
  // Priors for f.e
  target += beta_lpdf(P_ref_NP1 | 1, 1);
  // Priors for r.e.
  target += normal_lpdf(tau_u | 0, 2) -
     normal_lccdf(0 | 0, 2);
  target += normal_lpdf(tau_w | 0, 2) -
    normal_lccdf(0 | 0, 2);
  target += std_normal_lpdf(z_u);
  target += std_normal_lpdf(z_w);
}
generated quantities {
  int NP1_pred[N_pron];
  real loglik[N_pron];
  real P_expect_dat = inv_logit(alpha_NP1); //there is only dative
  vector[N_pron] lo_expect_NP1= alpha_NP1 + u[subj_pron] + w[item_pron];
  for(n in 1:N_pron){
    NP1_pred[n] = bernoulli_logit_rng(lo_expect_NP1[n]);
    loglik[n] = bernoulli_logit_lpmf(NP1_pron[n] | lo_expect_NP1[n]);
   }
}
