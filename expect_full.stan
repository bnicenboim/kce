data {
  //for fitting
  int N_free; //obs in the free promt data
  int NP1_free[N_free]; // referent == 1 in free promt data
  int N_subj; // N subj in free prompt data
  int N_item; // total number of subject
  vector[N_item] vtypej; // either accusative ==1, or dat ==-1 for each item
  int<lower = 1, upper = N_subj> subj_free[N_free]; // indexes subjects
  int<lower = 1, upper = N_item> item_free[N_free]; // indexes items
  //for prediction
  int N_pron; //obs in the pronoun prompt data
  int NP1_pron[N_pron]; // referent == 1 in pronoun promt data
  int<lower = 1, upper = N_item> item_pron[N_pron]; // indexes items in the pronoun prompt data
  int<lower = 1, upper = N_subj> subj_pron[N_pron]; // indexes subjects
}
parameters {
  real<lower = 0, upper = 1> P_ref_NP1; // P(referent==1)
  real beta_vtype; // difference between vtypes in logodds
  // Random effects:
  vector<lower = 0>[2]  tau_u; // by subject variance component
  real<lower = 0> tau_w; // by items variance component
  matrix[2, N_subj] z_u;
  vector[N_item] z_w;
  cholesky_factor_corr[2] L_u;
}
transformed parameters {
  matrix[N_subj, 2] u; // adjustments for subjects
  vector[N_item] w; // adjustment for item
  real alpha_NP1 = logit(P_ref_NP1); //log odds of referent == 1
  vector[N_free] lo_ref; //log odds of referent == 1
  u = (diag_pre_multiply(tau_u, L_u) * z_u)';
  w = tau_w * z_w;
  lo_ref = (alpha_NP1 + u[subj_free, 1] + w[item_free]) +
    vtypej[item_free] .*(beta_vtype + u[subj_free, 2]);
  // glmer(ans ~ vtype + (1|item_free) + (vtype | subj_free))
}
model {
  target += bernoulli_logit_lpmf(NP1_free | lo_ref);
  // Priors for f.e
  target += beta_lpdf(P_ref_NP1 | 1, 1);
  target += normal_lpdf(beta_vtype | 0, 2);
  // Priors for r.e.
  target += normal_lpdf(tau_u | 0, 2) -
    2 * normal_lccdf(0 | 0, 2);
  target += normal_lpdf(tau_w | 0, 2) -
    normal_lccdf(0 | 0, 2);
  target += lkj_corr_cholesky_lpdf(L_u | 2);
  target += std_normal_lpdf(to_vector(z_u));
  target += std_normal_lpdf(z_w);
}
generated quantities {
  int NP1_pred[N_pron];
  corr_matrix[2] rho_u = L_u * L_u';
  real loglik[N_pron];
  real P_expect_acc = inv_logit( alpha_NP1 + beta_vtype); //acc coded as 1
  real P_expect_dat = inv_logit( alpha_NP1 - beta_vtype); //dat coded as -1
  vector[N_pron] lo_expect_NP1=  alpha_NP1 + u[subj_pron, 1] + w[item_pron] +
    vtypej[item_pron] .* (beta_vtype+ u[subj_pron, 2]);

  for(n in 1:N_pron){
    NP1_pred[n] = bernoulli_logit_rng(lo_expect_NP1[n]);
    loglik[n] = bernoulli_logit_lpmf(NP1_pron[n] | lo_expect_NP1[n]);
   }
}
