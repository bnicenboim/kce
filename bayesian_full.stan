data {
   //for fitting
  int N_free; //obs in the free promt data
  int NP1_free[N_free]; // referent == 1 in free promt data
  int pron_free[N_free]; // 1 is PP, 2 is DP, 3 is other
  int N_subj; // N subj in free prompt data
  int N_item; // total number of subject
  vector[N_item] vtypej; // either accusative ==1, or dat ==-1 for each item
  vector[N_free] ref_free; // either NP1 ==1, or NP2 ==-1 for each obs
  int<lower = 1, upper = N_subj> subj_free[N_free]; // indexes subjects
  int<lower = 1, upper = N_item> item_free[N_free]; // indexes items
  //for prediction
  int N_pron; //obs in the pronoun prompt data
  int NP1_pron[N_pron]; // referent == 1 in pronoun promt data
  int<lower = 1, upper = N_item> item_pron[N_pron]; // indexes items in the pronoun prompt data
  int<lower = 1, upper = N_subj> subj_pron[N_pron]; // indexes subjects
  vector[N_pron] ref_pron; // either NP1 ==1, or NP2 ==-1 for each obs
  int pp_pron[N_pron];
}
parameters {
  real<lower = 0, upper = 1> P_ref_NP1;
  //fixed effects for all the different log-odds
  real alpha[2];
  real beta_vtype[3];
  real beta_ref[2];
  real beta_int[2];
  // Random effects:
  vector<lower = 0>[10] tau_u;
  vector<lower = 0>[5] tau_w;
  matrix[10, N_subj] z_u;
  matrix[5, N_item] z_w;
  cholesky_factor_corr[10] L_u;
  cholesky_factor_corr[5] L_w;
}
transformed parameters {
  matrix[N_subj, 10] u= (diag_pre_multiply(tau_u, L_u) * z_u)';
  matrix[N_item, 5] w = (diag_pre_multiply(tau_w, L_w) * z_w)';
  vector[N_free] lo_pp;
  vector[N_free] lo_other;
  vector[N_free] lo_ref;
  real alpha_NP1 = logit(P_ref_NP1);

  
  //log(theta_PP/theta_DP)
  lo_pp = (alpha[1]  + u[subj_free, 1] + w[item_free,1]) +
    vtypej[item_free] .*(beta_vtype[1] + u[subj_free, 2]) +
    ref_free .* (beta_ref[1] + u[subj_free, 3] + w[item_free,2]) +
    vtypej[item_free] .*ref_free .* (beta_int[1] + u[subj_free, 4]);

  //log(theta_other/theta_DP)
  lo_other = (alpha[2]  + u[subj_free, 5] + w[item_free,3]) +
   vtypej[item_free] .*(beta_vtype[2] + u[subj_free, 6]) +
   ref_free .* (beta_ref[2] + u[subj_free, 7] + w[item_free,4]) +
   vtypej[item_free] .*ref_free .* (beta_int[2] + u[subj_free, 8]);
//log(theta_ref/(1-theta_ref))
  lo_ref = (alpha_NP1 + u[subj_free, 9] + w[item_free,5]) +
    vtypej[item_free] .*(beta_vtype[3] + u[subj_free, 10]);

}
model {
  target += bernoulli_logit_lpmf(NP1_free | lo_ref);
  for(i in 1:N_free)
     target += categorical_logit_lpmf(pron_free[i]| [lo_pp[i], 0.0, lo_other[i]]' );
  // Priors for f.e
  target += normal_lpdf(alpha| 0, 2);
  target += beta_lpdf(P_ref_NP1 | 1, 1);
  target += normal_lpdf(beta_vtype | 0, 2);
  target += normal_lpdf(beta_ref | 0, 2);
  target += normal_lpdf(beta_int | 0, 2);
  // Priors for r.e.
  target += normal_lpdf(tau_u | 0, 2) -
    10 * normal_lccdf(0 | 0, 2);
  target += normal_lpdf(tau_w | 0, 2) -
    5* normal_lccdf(0 | 0, 2);
  target += lkj_corr_cholesky_lpdf(L_u | 2);
  target += lkj_corr_cholesky_lpdf(L_w | 2);
  target += std_normal_lpdf(to_vector(z_u));
  target += std_normal_lpdf(to_vector(z_w));
}
generated quantities {
  corr_matrix[10] rho_u = L_u * L_u';
  corr_matrix[5] rho_w = L_w * L_w';
  int NP1_pred[N_pron];
  real loglik[N_pron];
  real P_ref_acc = inv_logit(alpha_NP1 + beta_vtype[2]); //acc coded as 1
  real P_ref_dat = inv_logit(alpha_NP1 - beta_vtype[2]); //dat coded as -1
 // acc =1, ref np1 = 1
 vector[3] pron_ref1_acc = softmax([alpha[1]   +
                                    beta_vtype[1] +
                                    beta_ref[1]  +
                                    beta_int[1],
                                      0,
                                      alpha[2]   +
                                    beta_vtype[2] +
                                    beta_ref[2]  +
                                    beta_int[2]]');

  vector[3] pron_ref2_acc = softmax([alpha[1]   +
                                     beta_vtype[1] +
                                     - beta_ref[1] +
                                     - beta_int[1],
                                     0,
                                     alpha[2]   +
                                     beta_vtype[2] +
                                     - beta_ref[2] +
                                     - beta_int[2]]');

  real P_mirror_pp_acc = pron_ref1_acc[1]/(pron_ref1_acc[1] + pron_ref2_acc[1]);
  real P_mirror_dp_acc = pron_ref1_acc[2]/(pron_ref1_acc[2] + pron_ref2_acc[2]);

 vector[3] pron_ref1_dat = softmax([alpha[1] +
                                    - beta_vtype[1] +
                                    beta_ref[1]  +
                                    - beta_int[1],
                                      0,
                                      alpha[2]   +
                                    - beta_vtype[2] +
                                    beta_ref[2]  +
                                    - beta_int[2]]');

  vector[3] pron_ref2_dat = softmax([alpha[1]   +
                                     - beta_vtype[1] +
                                     - beta_ref[1] +
                                     beta_int[1],
                                     0,
                                     alpha[2]   +
                                     - beta_vtype[2] +
                                     - beta_ref[2] +
                                     beta_int[2]]');


  real P_bayes_pp_acc = pron_ref1_acc[1]* P_ref_acc /
    (pron_ref1_acc[1] * P_ref_acc  + pron_ref2_acc[1] * (1- P_ref_acc));
  real P_bayes_dp_acc = pron_ref1_acc[2]* P_ref_acc/
    (pron_ref1_acc[2]* P_ref_acc + pron_ref2_acc[2]*(1- P_ref_acc));


  real P_bayes_pp_dat = pron_ref1_dat[1]* P_ref_dat /
    (pron_ref1_dat[1]* P_ref_dat  + pron_ref2_dat[1] * (1- P_ref_dat));
  real P_bayes_dp_dat = pron_ref1_dat[2]* P_ref_dat/
    (pron_ref1_dat[2]* P_ref_dat + pron_ref2_dat[2]*(1- P_ref_dat));

vector[N_pron] P_ref1 = inv_logit(alpha_NP1 + u[subj_pron, 9] + w[item_pron,5] + vtypej[item_pron] .* (beta_vtype[3]+ u[subj_pron, 10]));

for(n in 1:N_pron){
  vector[3] pron_ref1 = softmax(
                                [alpha[1]  + u[subj_pron[n], 1] + w[item_pron[n],1] +
                                 vtypej[item_pron[n]] .*(beta_vtype[1] + u[subj_pron[n], 2]) +
                                 (beta_ref[1] + u[subj_pron[n], 3]+ w[item_pron[n],2] ) +
                                 vtypej[item_pron[n]]  .* (beta_int[1] + u[subj_pron[n], 4]),

                                 0,

                                 alpha[2]  + u[subj_pron[n], 5] + w[item_pron[n],3] +
                                 vtypej[item_pron[n]] .*(beta_vtype[2] + u[subj_pron[n], 6]) +
                                 (beta_ref[2] + u[subj_pron[n], 7] + w[item_pron[n],4] ) +
                                 vtypej[item_pron[n]]  .* (beta_int[2] + u[subj_pron[n], 8])]'
                                );

  vector[3] pron_ref2 = softmax(
                                [alpha[1]  + u[subj_pron[n], 1] + w[item_pron[n],1] +
                                 vtypej[item_pron[n]] .*(beta_vtype[1] + u[subj_pron[n], 2]) +
                                 -(beta_ref[1] + u[subj_pron[n], 3]+ w[item_pron[n],2] ) +
                                 -vtypej[item_pron[n]]  .* (beta_int[1] + u[subj_pron[n], 4]),

                                 0,

                                 alpha[2]  + u[subj_pron[n], 5] + w[item_pron[n],3] +
                                 vtypej[item_pron[n]] .*(beta_vtype[2] + u[subj_pron[n], 6]) +
                                 -(beta_ref[2] + u[subj_pron[n], 7]+ w[item_pron[n],4] ) +
                                 -vtypej[item_pron[n]]  .* (beta_int[2] + u[subj_pron[n], 8])]'
                                );

  // "likelihood for personal pronoun"
      real P_pp = pron_ref1[1] * P_ref1[n]/
        (pron_ref1[1] * P_ref1[n] + pron_ref2[1] * (1-P_ref1[n]));
      real P_dp = pron_ref1[2] * P_ref1[n] /
        (pron_ref1[2] * P_ref1[n] + pron_ref2[2] * (1-P_ref1[n]));

        if(pp_pron[n]==1){
          loglik[n] = bernoulli_lpmf(NP1_pron[n] | P_pp);
          NP1_pred[n] = bernoulli_rng(P_pp);
        }else{
          loglik[n] = bernoulli_lpmf(NP1_pron[n] | P_dp);
          NP1_pred[n] = bernoulli_rng(P_dp);
        }
   }

}
